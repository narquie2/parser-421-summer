# parser-421-summer

## Name
Derivative Parser (CS 421 2022 Summer)

## Description
This project contains python and java work on partial implementation of the Ian, Gianfranco, and Keshav paper.

The python code explanation can be found in the mp4 file in the "Derivative Grammars" folder.

Java code sourced from https://gist.github.com/blazs/2c33b8be5add79211c4354ff55f9d226

## Installation
Python code is tested to work on 3.7.9

## Usage
Python code: Add text to a text document per the format in the text files found in "Derivative Grammars" folder.

Format: Grammar first, each production separated left hand side (LHS) and right hand side (RHS) by an & sign. Epsilon marked by @.

To run, add runDerive("\<FILENAME>\.txt") to the bottom of the python code.

To unit test, run Derivation_Test.py.

Code prints out the input text, then the grammar for each iteration, and finally a boolean whether the answer was reachable or not.

## Authors and acknowledgment
narquie2, ashas4
