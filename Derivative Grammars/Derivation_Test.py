import unittest
from Derivation import Production
from Derivation import Grammar
from Derivation import terminable
from Derivation import evalProd
from Derivation import iterEval
from Derivation import derive

class TestStringMethods(unittest.TestCase):
    def test_prod_functionality(self):
        prod = Production("E & a E a")
        self.assertEqual(prod.itemized,["a","E","a"])
        self.assertEqual(prod.nonterminals,["E"])
        self.assertEqual(prod.terminals,["a","a"])
    def test_prod_empty(self):
        prod = Production("E & @")
        self.assertEqual(prod.itemized,[])
        self.assertEqual(prod.nonterminals,[])
        self.assertEqual(prod.terminals,[])
    def test_grammar(self):
        gramm = Grammar(["E & a E a","E & b E b","E & @"])
        self.assertEqual(len(gramm.prods["E"]),3)
        self.assertEqual(str(gramm).split("\n"),"E & a E a\nE & b E b\nE & @\n".split("\n"))
    def test_terminable(self):
        prod = Production("E & @")
        gramm = Grammar(["E & a E a","E & b E b","E & @"])
        self.assertTrue(terminable(prod,gramm))
    def test_solvable(self):
        prod = Production("E & a E a")
        gramm = Grammar(["E & a E a","E & b E b","E & @"])
        self.assertTrue(evalProd(prod,gramm,"aa",0,True))
    def test_iterSolvable(self):
        prod = Production("E & a E a")
        gramm = Grammar(["E & a E a","E & b E b","E & @"]) 
        self.assertTrue(iterEval(prod,gramm,"a",0))
    def test_deriveGrammar(self):
        gramm = Grammar(["E & a E a","E & b E b","E & @"])
        newGramm = derive(gramm,0,"aa")
        truthGramm = Grammar(["E & a E a","E & b E b","E & @","Ea & E a","Eaa & @","Eaa & Ea a"])
        self.assertEqual(str(newGramm).split("\n"),str(truthGramm).split("\n"))

if __name__ == '__main__':
    unittest.main()

        