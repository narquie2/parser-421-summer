class Production(object):
    def __init__(self, text):
        self.text = text
        self.name = ""
        self.terminals = []
        self.nonterminals = []
        self.itemized = []
        self.tokenize()
    # Take apart (N U T)

    def tokenize(self):
        self.terminals = []
        self.nonterminals = []
        self.itemized = []
        self.text = self.text.strip()
        splitText = self.text.split(" ")
        self.name = splitText[0]
        for token in splitText[1:]:
            if token == "@":
                pass
            elif token[0].isupper():
                self.nonterminals.append(token)
                self.itemized.append(token)
            elif token[0].islower():
                self.terminals.append(token[0])
                self.itemized.append(token[0])
    # Helper function for deriving
    # Derive1 uses new nonterminal

    def derive(self, dName):
        sepText = self.text.split(" ")
        newText = ""
        count = 0
        # Check if each matches name for derive
        for token in sepText:
            # Create derivation if first right hand nonterminal not touched yet
            # Token must begin with same letter
            if token[0] == self.name[0] and count < 2:
                newText += token + dName
                if count == 0:
                    newText += " & "
                count += 1
            # Keep all tokens after first matching nonterminal checked
            elif count == 2:
                newText += " " + token
        # Reconstruct production
        if count <= 1:
            return False
        self.text = newText
        self.tokenize()

    def deriveLow(self, dName):
        sepText = self.text.split(" ")
        newText = ""
        count = 0
        # Check if each matches name for derive
        for token in sepText:
            # Create derivation if first right hand nonterminal not touched yet
            # Token must begin with same letter
            if token[0] == self.nonterminals[0] or token[0] == self.name[0] and count < 2:
                if count == 0:
                    newText += self.name + dName
                    newText += " & "
                else:
                    newText += token
                count += 1
            # Keep all tokens after first matching nonterminal checked
            elif count == 2:
                newText += " " + token
        # Reconstruct production
        if count <= 1:
            return False
        self.text = newText
        self.tokenize()
    # Helper function for deriving
    ## Derive2 is self-referential or empty

    def deriveEmpty(self, dName):
        self.text = self.name + dName + " & @"
        self.tokenize()

    def deriveNeutral(self, dName):
        self.text = self.name + dName + " & " + self.name[0]
        self.tokenize()

    def __eq__(self, other):
        return (self.itemized == self.itemized)

    def __str__(self):
        return self.text


class Grammar(object):
    def __init__(self, text):
        self.prods = self.initGrammar(text)
    # Separate productions by nonterminal state

    def initGrammar(self, text):
        productionsDict = {}
        for product in text:
            currProd = Production(product)
            if currProd.name in productionsDict:
                productionsDict[currProd.name].append(currProd)
            else:
                productionsDict[currProd.name] = [currProd]
        return productionsDict

    def __str__(self):
        text = ""
        for key in self.prods.keys():
            for prod in self.prods[key]:
                if str(prod)[-1] != "\n":
                    text += str(prod) + "\n"
                else:
                    text += str(prod)
        return text


def derive(gramm, index, inputText):
    print(gramm)
    # Recursive base case: text has been read in full
    if index == len(inputText):
        return gramm
    else:
        currToken = inputText[index]
    emptyList = {}
    toPop = []
    fullProds = []
    deriveLater = []
    for key in gramm.prods.keys():
        emptyList[key] = Production("")
    for key in gramm.prods.keys():
        addtlProds = []
        empty = False
        for prod in gramm.prods[key]:
            # Derive grammar by adding text to nonterminal
            for token in prod.itemized:
                if len(prod.nonterminals) > 0:
                    prodNew = Production(prod.text)
                    addtlProds.append(prodNew)
                    break
            # Is it solvable? Add a termination
            if not empty and not terminable(prod, gramm):
                if len(emptyList[key].name) < len(prod.name):
                    prodListEmpty = Production(prod.text)
                    emptyList[key] = prodListEmpty
            elif terminable(prod, gramm):
                empty = True
                emptyList[key] = Production("")
        # Is it not solvable? Use basic symbol
        for p in addtlProds:
            if (evalProd(p, gramm, p.name[1:]+currToken, 0, False) and not empty):
                dProd3 = Production(p.text)
                dProd3.deriveNeutral(currToken)
                fullProds.append([dProd3.name, dProd3])
                # toPop.append([key,p.itemized])
        for p in addtlProds:
            prodDerive = Production(p.text)
            if prodDerive.itemized[0][0].islower():
                prodDerive.deriveLow(currToken)
            else:
                prodDerive.derive(currToken)
            if len(p.name) == 1:
                if iterEval(p, gramm, currToken, 0):
                    fullProds.append([prodDerive.name, prodDerive])
                else:
                    deriveLater.append([prodDerive.name, prodDerive])
            else:
                fullProds.append([prodDerive.name, prodDerive])
                toPop.append([key, p.itemized])
                # gramm.prods[key].append(prodDerive)

        # for p in addtlProds:
        #   prodLow = Production(p.text)
        #   prodLow.deriveLow(currToken)
        #   if evalProd(prodLow,gramm,prodLow.name[1:],0):
        #     fullProds.append([key,prodLow])

    for key in emptyList.keys():
        # This might fail...
        # Make the most basic symbol solvable that is solvable
        prod = emptyList[key]
        nameCompare = prod.name[1:] + currToken
        alreadyEmpty = False
        if prod.text != "":
            if evalProd(prod, gramm, prod.name[1:], 0, False) and nameCompare == inputText[:len(nameCompare)]:
                for p in gramm.prods[key]:
                    if len(p.itemized) == 0:
                        alreadyEmpty = True
                if not alreadyEmpty:
                    emptyProd = Production(prod.text)
                    emptyProd.deriveEmpty(currToken)
                    fullProds.append([key, emptyProd])

    for ele in deriveLater:
        if ele[0] not in gramm.prods:
            gramm.prods[ele[0]] = []

    for ele in fullProds:
        if ele[0] not in gramm.prods:
            gramm.prods[ele[0]] = []

    addKeys = []
    for key in gramm.prods.keys():
        addKeys.append(key+currToken)
    for key in addKeys:
        if key not in gramm.prods:
            gramm.prods[key] = []

    for key in gramm.prods.keys():
        neutralDone = False
        for prod in gramm.prods[key]:
            if terminable(prod, gramm):
                if len(prod.name) > 1 and not neutralDone:
                    prodNeutral = Production(prod.text)
                    prodNeutral.deriveNeutral(currToken)
                    nameCompare = prodNeutral.name[1:]
                    if nameCompare == inputText[:len(nameCompare)]:
                        if prodNeutral.text != "":
                            gramm.prods[prodNeutral.name].append(prodNeutral)
                            toPop.append([key, prod.itemized])
                            neutralDone = True

    for ele in fullProds:
        if ele[1].text != "":
            gramm.prods[ele[0]].append(ele[1])

    for ele in toPop:
        key = ele[0]
        p = ele[1]
        currIndex = -1
        for i, gP in enumerate(gramm.prods[key]):
            if gP.itemized == p:
                currIndex = i
        if currIndex != -1:
            gramm.prods[key].pop(currIndex)

    return derive(gramm, index+1, inputText)


def evalProd(prod, grammar, inputText, index, fullText):
    # Is it Terminable?
    if index == 0 and terminable(prod, grammar):
        return True
    results = []
    # Has / will more text be covered then needs to be parsed?
    if len(prod.terminals) + index <= len(inputText):
        for loc, token in enumerate(prod.itemized):
            # If you would add to full text, don't
            if index >= len(inputText) and token.islower():
                return False
            # If token matches, continue
            elif token.islower() and token == inputText[index]:
                index += 1
            elif token.islower() and token != inputText[index]:
                return False
            # If token is nonterminal, evalProd the next prod(s) it points to
            elif token[0].isupper():
                for symbol in grammar.prods.keys():
                    if token == symbol:
                        for prodCont in grammar.prods[symbol]:
                            if not prodCont.itemized:
                                pNew = Production("S &")
                                for ele in prod.itemized[loc+1:]:
                                    pNew.text += " " + ele
                                pNew.tokenize()
                                results.append(
                                    evalProd(pNew, grammar, inputText, index, False))
                            elif len(prodCont.itemized) > 0:
                                if len(prodCont.nonterminals) > 0:
                                    if prod.name != prodCont.itemized[0]:
                                        prodEmpty = Production(prodCont.text)
                                        for ele in prodCont.itemized[loc+1:]:
                                            prodEmpty.text += " " + ele
                                        for ele in prod.itemized[loc+1:]:
                                            prodEmpty.text += " " + ele
                                        prodEmpty.tokenize()
                                        results.append(
                                            evalProd(prodEmpty, grammar, inputText, index, False))
                break
    # If everything is complete, great! complete it.
    elif len(prod.itemized) == 0 and len(inputText) == index:
        return True
    # If none of the above apply, return false
    else:
        return False
    # If at least one branch succeeds, succeed.
    for i in results:
        if i:
            return True
    if len(results) == 0 and len(inputText) == index:
        return True
    return False


def terminable(prod, grammar):
    results = []
    if len(prod.itemized) == 0:
        return True
    if len(prod.terminals) > 0:
        return False
    if len(prod.nonterminals) > 1:
        return False
    results = []
    for loc, token in enumerate(prod.nonterminals):
        for symbol in grammar.prods.keys():
            if token == symbol:
                for prodCont in grammar.prods[symbol]:
                    if len(prodCont.itemized) == 0:
                        return True
                    elif len(prodCont.nonterminals) == 1 and len(prodCont.terminals) == 0:
                        if prod.name != prodCont.itemized[0]:
                            results.append(terminable(prodCont, grammar))
    for fact in results:
        if fact:
            return True
    return False


def iterEval(prod, grammar, inputText, index):
    if index == 0 and prod.name[1:].strip() == inputText.strip():
        return True
    results = []
    # Has / will more text be covered then needs to be parsed?
    if index <= len(inputText):
        for loc, token in enumerate(prod.itemized):
            # If you would add to full text, don't
            if index >= len(inputText) and token.islower():
                return False
            # If token matches, continue
            elif token.islower() and token == inputText[index]:
                index += 1
                if index == len(inputText):
                    return True
            elif token.islower() and token != inputText[index]:
                return False
            # If token is nonterminal, evalProd the next prod(s) it points to
            elif token[0].isupper():
                for symbol in grammar.prods.keys():
                    if token == symbol:
                        for prodCont in grammar.prods[symbol]:
                            if not prodCont.itemized:
                                if prod.itemized[loc] == inputText:
                                    return True
                            elif len(prodCont.itemized) > 0:
                                if prodCont.itemized[0].islower():
                                    if prodCont.itemized[0] == inputText:
                                        return True
                                elif len(prodCont.nonterminals) > 0:
                                    if prod.name != prodCont.nonterminals[0]:
                                        prodEmpty = Production(prodCont.text)
                                        for ele in prodCont.itemized[loc+1:]:
                                            prodEmpty.text += " " + ele
                                        for ele in prod.itemized[loc+1:]:
                                            prodEmpty.text += " " + ele
                                        prodEmpty.tokenize()
                                        results.append(
                                            iterEval(prodEmpty, grammar, inputText, index))
                break
    # If everything is complete, great! complete it.
    elif len(prod.itemized) == 0 and len(inputText) == index:
        return True
    # If none of the above apply, return false
    else:
        return False
    # If at least one branch succeeds, succeed.
    for i in results:
        if i:
            return True
    if len(results) == 0 and len(inputText) == index:
        return True
    return False


def runDerive(fileName):
    inputText = ""
    grammars = []
    with open(fileName) as f:
        lines = f.readlines()

    for line in lines:
        if line.strip() == "":
            pass
        elif line[0] != "w":
            grammars.append(line)
        else:
            inputText = line[4:].strip()
            print(inputText)
    f.close()
    newGrammar = Grammar(grammars)
    derivedGrammar = derive(newGrammar, 0, inputText)
    priorityQueue = [0, ""]
    # for key in derivedGrammar.prods.keys():
    # if len(key) > priorityQueue[0]:
    # priorityQueue = [len(key),key]
    isTrue = False
    for key in derivedGrammar.prods.keys():
        # for prod in derivedGrammar.prods[priorityQueue[1]]:
        for prod in derivedGrammar.prods[key]:
            if evalProd(prod, derivedGrammar, inputText, 0, True):
                isTrue = True
                break
    print(isTrue)

if __name__ == '__main__':
    runDerive("aEa2.txt")
    runDerive("n+n+n.txt")
    runDerive("axa.txt")
    runDerive("dcece.txt")
    runDerive("axvba+a.txt")
    runDerive("37379k.txt")